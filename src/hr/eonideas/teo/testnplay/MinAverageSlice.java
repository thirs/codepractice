package hr.eonideas.teo.testnplay;

import java.util.Random;

public class MinAverageSlice {
	private static Solution s;
	private static Random generator;
	
	public static void main (String[] args) {
		s = new Solution();
		generator = new Random();	
		//Tester methods 
//		testCases();
		
		int result = s.solution(new int[] {12,333,51,1111,1235,5,1233,3,4,124,1246,1213,123}); //should be 7 => 3+4/2 = 3.5
		System.out.println("Index is " + result);
	}
	
	
	//HELPER Methos
	private static int[] initializeArray(int arrayLenght, int valueMin, int valueMax) {
		int[] array = new int[arrayLenght];
		for(int i = 0; i < arrayLenght; i++) {
			array[i] = generateNumber(valueMin, valueMax);
		}
		return array;
	}
	
	private static int generateNumber(int valueMin, int valueMax) { 
		int i = generator.nextInt(valueMax) + valueMin;
		return i;
	}

	//TESTING Methods	
	private static void testCases() {
		try {
			s.solution(new int[] {-1000000000, -1000000000, -1000000000});	
		}catch(IllegalArgumentException e) {
			System.out.println("numberOutOfRange--------OK");
		}
		try {
			s.solution(new int[] {5});
		}catch(IllegalArgumentException e) {
			System.out.println("singleItemedArray--------OK");
		}
		try {
			s.solution(new int[] {});
		}catch(IllegalArgumentException e) {
			System.out.println("emptyArray--------OK");
		}
		try {
			s.solution(null);
		}catch(IllegalArgumentException e) {
			System.out.println("nullArray--------OK");
		}

		try{
			int validCase_0 = s.solution(initializeArray(10000, -10000, 10000));
			System.out.println("validCase_0 => initializeArray(10000, -10000, 10000)--------OK");
		}catch(IllegalArgumentException e){
			System.out.println("validCase_0 => initializeArray(10000, -10000, 10000)--------NOK");
		}
		int validCase_1 = s.solution(new int[] {1,2,3,4,0,0});
		System.out.println("validCase_1 => int[] {1,2,3,4,0,0}--------"+((validCase_1 == 4) ? "OK" : "NOK"));
		int validCase_2 = s.solution(new int[] {0,0,0,0,0,0});
		System.out.println("validCase_2 => int[] {0,0,0,0,0,0}--------"+((validCase_2 == 0) ? "OK" : "NOK"));
		int validCase_3 = s.solution(new int[] {5,10});
		System.out.println("validCase_3 => int[] {5,10}--------"+((validCase_3 == 0) ? "OK" : "NOK"));
		int validCase_4 = s.solution(new int[] {4,2,2,5,1,5,8});
		System.out.println("validCase_4 => int[] {4,2,2,5,1,5,8}--------"+((validCase_4 == 1) ? "OK" : "NOK"));
		
	}
	
	private static void testHelperMethods() {
		System.out.println("---------TEST STARTED------");
		System.out.println(" ");
		double av = s.getAverageValue(-0.5, 39, 2);
		System.out.println("Test getAverageValue method------"+ ((av == 12.67) ? "OK" : "NOK"));
		
		int gn = generateNumber(-10000, 10000);
		System.out.println("Test generateNumber method------"+ ((-10000 <= gn && gn <= 10000) ? "OK" : "NOK"));
		System.out.println("Initializing array------");
		int[] inputArray = initializeArray(20, -10000, 10000);
		int check = 1;
		check &= (inputArray.length == 20) ? 1 :0;
		for(int i = 0; i<inputArray.length; i++) {
			check &= (-10000 < inputArray[i] && inputArray[i]< 10000) ? 1 :0;
			System.out.println("A["+i+"]="+inputArray[i]);
		}
		System.out.println("Test initializeArray method------"+ ((check == 1) ? "OK" : "NOK"));
		System.out.println("---------TEST FINISHED------");
	}
}
