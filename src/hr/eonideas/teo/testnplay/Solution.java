package hr.eonideas.teo.testnplay;

public class Solution {
	
	private static final int MAX_ARRAY_LENGTH = 100000;
	private static final int MIN_ARRAY_LENGTH = 2;
	private static final int MAX_VALUE = 10000;
	private static final int MIN_VALUE = -10000;
	
	public int solution(int[] A) throws IllegalArgumentException{
		if(A == null || A.length < MIN_ARRAY_LENGTH || A.length > MAX_ARRAY_LENGTH) {
			throw new IllegalArgumentException("Array should not be null and should contain at least two items but not more than 100,000 items");
		}
		int firstIndex = -1; //starting position of a slice with min average
		double minAverage = -1; //starting minAverage
		int minSliceLength = 2; //minimum slice length is 2
		
		validateNumber(A[0]);
		//iterate through array 
		for(int i = 0; i <= A.length-minSliceLength; i++ ) {
			//initial slice of each arrayItem
			int endIndex = i + (minSliceLength-1);//end index of default slice (0,endIndex)
			double tempAverage = getAverageValue((double)A[i], A[i+1], minSliceLength);//initial tempAverage for the first slice;
			int tempSum = A[i] + A[i+1];//sum of initial slice of two array items
			//update smallest average properties only in first round or when new average is smaller than the current one 
			if(i == 0 || minAverage > tempAverage) {
				minAverage =  tempAverage;
				firstIndex = i;
			}
			
			//go through all slices where element indexed with i is first element in slice 
			for(int j = 1; j + endIndex < A.length; j++) {
				//increase element with one element
				int nextArrayItem = A[j + endIndex];
				tempAverage = getAverageValue(tempSum, nextArrayItem, j + endIndex +1 -i);
				tempSum += nextArrayItem;//after calculation, update new temporary sum which will be used in next slice increase
				
				//update smallest average properties only in first round or when new average is smaller than the current one 
				if(minAverage > tempAverage) {
					minAverage =  tempAverage;
					firstIndex = i;
				}
			}
		}			
		return firstIndex;
	};	


	//HELPER Methods
	//improved average calculation based on previous average and next array item
	public double getAverageValue(double previousAverage, int b, int sliceSize) throws IllegalArgumentException {
		validateNumber(b);
		double sum = previousAverage+b;
		float average = (float)sum/sliceSize;
		return Math.round(average * 100.0) / 100.0;
	}
	
	public void validateNumber(int b) {
		if(b > MAX_VALUE || b < MIN_VALUE) {
			throw new IllegalArgumentException("Array contains number out of predefined range (N is an integer within the range [-10,000..10,000])");
		}
	}
}
